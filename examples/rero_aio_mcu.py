# MIT License

# Copyright (c) 2019 Reinhard Panhuber

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import asyncio
import aioserial
import logging
import crcmod

try:
    import rero
except ImportError:
    from src.rero import packer as rero     # to work in PyCharm locally


async def aio_recv_and_unpack(ser: aioserial.AioSerial, reroPacker: rero.Packer) -> bytes:
    """Async. read of all received bytes and unpack."""

    payload = b''

    while not payload:

        # Await the first byte
        msg = await ser.read_async()

        # Check if there is more to read
        msg = msg + ser.read(ser.in_waiting)

        # Unpack and return
        for payload in reroPacker.unpack(msg):

            # Since we enabled yielding of errors we need to check if stuff returned is of an error type
            if isinstance(payload, rero.ReroError):
                raise payload

    return payload


async def main(ser: aioserial.AioSerial, reroPacker: rero.Packer) -> None:
    # Communication testing messages

    # Normal message where COBS applies
    payload = bytes([1, 2, 3, 0, 4, 5, 6, 0, 7, 8, 1])
    msgCOBS = reroPacker.pack(payload, 1)

    # Send
    ser.write(msgCOBS)
    print('Sent normal message COBS...')

    # Receive answer from MCU
    try:
        payloadRcd = await asyncio.wait_for(aio_recv_and_unpack(ser, reroPacker), 1)     # timeout 1 sec
        print('Answer received...')
        # Check if received frame is the same
        if payload == payloadRcd:
            print('Success!')
        else:
            print('Test failed!')
    except asyncio.TimeoutError:
        print('No answer received!')

    except rero.ReroError as e:
        print(e)


if __name__ == "__main__":

    import sys

    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    # Setup logging
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # Setup rero
    reroPacker = rero.Packer(addrSrc=99, addrDstDefault=1, crc=crcmod.predefined.Crc('crc-32-mpeg'), addrFieldLen=2,
                            addFrameFieldLen=1, enableFrameLenCheck=True, yieldCRCError=True,
                            yieldOtherRecipientNotification=True, logHandler=ch)

    ser = aioserial.AioSerial()
    ser.baudrate = 38400
    ser.port = 'COM6'       # PC
    # ser.port = 'COM3'  # Laptop

    with ser as ser:
        asyncio.run(main(ser, reroPacker))
